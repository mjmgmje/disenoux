import * as React from 'react';

interface ITableProps {
    data: any
}

interface ITableState {
    data: any
}

class TableComp extends React.Component<ITableProps, ITableState> {

    constructor(props: ITableProps) {
        super(props);
    }

    public createTable(data: any) {
        let table = []

        // Outer loop to create parent
        for (let i = 0; i < data.length; i++) {
            let children = []
            //Inner loop to create children
            if (i == 0) {
                for (let j = 0; j < data[i].length; j++) {
                    children.push(<td>{data[i][j]}</td>)
                }
            } else {
                for (let j = 0; j < data[i].length - 1; j++) {
                    if (j == 2) {
                        children.push(<td><a href={data[i][j + 1]}>{data[i][j]}</a></td>)
                    } else {
                        children.push(<td>{data[i][j]}</td>)
                    }
                }
            }
            //Create the parent and add the children
            table.push(<tr>{children}</tr>)
        }
        return table
    }

    public render() {
        const table = this.createTable(this.props.data);
        return (
            <table className="table-responsive table-stripped">
                <tbody>
                    {table}
                </tbody>
            </table>
        )
    }

}
export default TableComp;


