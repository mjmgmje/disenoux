import * as React from 'react';

interface ICardTitleProps {
    data: any
}

interface ICardTitleState {
    data: any
}

class CardTitleComp extends React.Component<ICardTitleProps, ICardTitleState> {

    constructor(props: ICardTitleProps) {
        super(props);
    }

    public render() {
        const { img, title, description } = this.props.data;
        return (
            <div className="prog-title-frame">
                <div className="prog-title">
                    <img src={img} />
                    <h2>{title}</h2>
                    <hr />
                    <div dangerouslySetInnerHTML={{ __html: description }} />
                 
                </div>
            </div>
        )
    }

}
export default CardTitleComp;

