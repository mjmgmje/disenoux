import * as React from 'react';  
import './../assets/css/style.css';  

interface IShareprops {
    text: String;
    link: String;
    closePopup: any;

}

interface IShareState {
}

class PopupShare extends React.Component<IShareprops, IShareState> {  
 
    constructor(props: IShareprops) {
        super(props);
        this.linkedinRedict = this.linkedinRedict.bind(this);
      }
    
    linkedinRedict(){
        const link = "https://www.linkedin.com/shareArticle?mini=true&url="+this.props.link+"&title=No te pierdas este podcast de la tecnologeria&summary=tecnologeria.com&source=tecnologeria";
        window.open(link, 'mywin',
        'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); 
        return false;
    }

    public render() {  
        const hrefTwitter = "https://twitter.com/intent/tweet?url=&text=No te pierdas este podcast de @Tecnologeria " + this.props.link;
        return (  
        <div className='cover-modal'>
            <div className='popup'>  
                <a className="closeButton" onClick={this.props.closePopup}><i className="fa fa-close"></i></a>
                <div className="shareButtons">
                    <a href={hrefTwitter} className="twitter-share-button" data-size="large" data-hashtags="#tecnologeria #libertad" data-lang="es" data-show-count="false"><i className="fa fa-twitter" ></i> Tweet</a><script async src="https://platform.twitter.com/widgets.js"></script>
                    <a onClick={this.linkedinRedict} className="linkedin-share-button"><img  src="https://chillyfacts.com/wp-content/uploads/2017/06/LinkedIN.gif" alt="" width="68" height="25" /></a>
                </div>
            </div>
        </div>  
        );  
    }  
}  

export default PopupShare;