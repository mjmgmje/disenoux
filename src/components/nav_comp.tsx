import * as React from 'react';

interface INavProps {

}

interface INavState {

}

class NavComp extends React.Component<INavProps, INavState> {

  constructor(props: INavProps) {
    super(props);
  }

  public render() {
    return (
      <div className="row dmbs-top-menu">
        <nav className="navbar navbar-fixed-top navbar-default" role="navigation">
          <a href="https://tecnologeria.com/">
            <img className="blog-header-image" src="./assets/images/Tecnologeria2.0.png" alt="" />
          </a>
          <div className="container">
            <div className="navbar-header">
              <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-1-collapse">
                <span className="sr-only">Cambiar de modo de navegación</span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
              </button>
            </div>

            <div className="collapse navbar-collapse navbar-1-collapse">
              <ul id="menu-principal" className="nav navbar-nav navbar-center">
                <li id="menu-item-7"
                  className="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-7 active">
                  <a title="&lt;i className=&quot;fa fa-lg fa-home&quot; aria-hidden=&quot;true&quot;&gt;&lt;/i&gt; Inicio"
                    href="https://tecnologeria.com/"><i className="fa fa-lg fa-home" aria-hidden="true"></i>
                    Inicio</a></li>
                <li id="menu-item-9"
                  className="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-9"><a
                    title="Episodios" href="https://tecnologeria.com/category/audios/">Episodios</a></li>
                <li id="menu-item-244"
                  className="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-244"><a
                    title="Blog" href="https://tecnologeria.com/category/noticias/">Blog</a></li>
                <li id="menu-item-359"
                  className="menu-item menu-item-type-post_type menu-item-object-page menu-item-359"><a
                    title="Recomendaciones"
                    href="https://tecnologeria.com/recomendamos/">Recomendaciones</a></li>
                <li id="menu-item-368"
                  className="menu-item menu-item-type-post_type_archive menu-item-object-event menu-item-368"><a
                    title="Eventos" href="https://tecnologeria.com/eventos/">Eventos</a></li>
                <li id="menu-item-14"
                  className="menu-item menu-item-type-post_type menu-item-object-page menu-item-14"><a
                    title="Equipo" href="https://tecnologeria.com/sobre-nosotros/">Equipo</a></li>
                <li id="menu-item-21"
                  className="menu-item menu-item-type-post_type menu-item-object-page menu-item-21"><a
                    title="Contacto" href="https://tecnologeria.com/contacto/">Contacto</a></li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    )
  }

}
export default NavComp;