import * as React from 'react';
import Card2Comp from './card2_comp';

interface ICCard2Props {
    data: any;
}

interface ICCard2State {
    data: any;

}

class ContCard2Comp extends React.Component<ICCard2Props, ICCard2State> {

    constructor(props: ICCard2Props) {
        super(props);
    }

    public render() {

        return (
            <div className="episodes-container">
                                
                {/* TARJETA 2*/}
                {
                    this.props.data.map((json: any, index: number) => (
                        <Card2Comp key={index} data={json}></Card2Comp>
                    ))
                }
            </div>

        )
    }

}
export default ContCard2Comp;