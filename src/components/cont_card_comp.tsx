import * as React from 'react';
import CardComp from './card_comp';
import CardTitleComp from './card_title_comp';

interface ICCardProps {
    data: any;
}

interface ICCardState {
    data: any;

}

class ContCardComp extends React.Component<ICCardProps, ICCardState> {

    constructor(props: ICCardProps) {
        super(props);
    }

    public render() {
        let jsonSel: any;
        let count=0;
        return (
            <div className="row progs">
                
                {/* TARJETA */}
                {
                     this.props.data.map((json: any, index: number) => {
                        
                        if(json["type"]=="complete"){
                            if(json["likes"]>=count){
                                jsonSel=json;
                                count = json["likes"];
                            }
                        }                            
                    }    
                    )
                }
                <CardComp key="1" data={jsonSel}></CardComp>
                {
                    this.props.data.map((json: any, index: number) => {
                        if(json["type"]=="complete"){
                            if (json != jsonSel){
                                return <CardComp key={index} data={json}></CardComp>;
                            }
                        }else{
                            return <CardTitleComp key={index} data={json}></CardTitleComp>;
                        }
                    }    
                    )
                }
            </div>

        )
    }

}
export default ContCardComp;