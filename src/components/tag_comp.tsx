import * as React from 'react';

interface ITagProps {
    data: any
}

interface ITagState {
    data: any
}

class TagComp extends React.Component<ITagProps, ITagState> {

    constructor(props: ITagProps) {
        super(props);
    }

    public render() {
        const { link, classNom, title, stitle } = this.props.data;
        return (
            <a href={link} className="playus-btn">
                <div className="playus-btn-header">
                    <div className="playus-btn-cell playus-btn-icon"><i className={classNom}
                        aria-hidden="true"></i></div>
                    <div className="playus-btn-cell">
                        <h4>{title}</h4>
                        <p>{stitle}</p>
                    </div>
                </div>
            </a>
        )
    }

}
export default TagComp;

