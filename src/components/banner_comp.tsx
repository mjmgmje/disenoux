import * as React from 'react';

interface IBanProps {

}

interface IBanState {

}

class BannerComp extends React.Component<IBanProps, IBanState> {

    constructor(props: IBanProps) {
        super(props);
    }

    public render() {
        return (
            <div className="frontpage-banner">
                <div className="frontpage-title text-center">
                    <div>
                        <h1>La Tecnologería</h1>
                        <hr style={{ width: '50px' }} />
                        <h3>La red de podcasts sobre informática y personas</h3>
                    </div>
                </div>
                <img src="./assets/images/banner.jpg" />
            </div>

        )
    }

}
export default BannerComp;