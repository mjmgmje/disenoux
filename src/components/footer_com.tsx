import * as React from 'react';

interface IFooterProps {
}

interface IFooterState {
}

class FooterComp extends React.Component<IFooterProps, IFooterState> {

    constructor(props: IFooterProps) {
        super(props);
    }

    public render() {

        return (
            <div className="tecnologeria-footer">
                <div className="container">
                    <div className="col-md-12 dmbs-footer-bar">
                        <aside id="text-3" className="col-md-6 widget widget_text">
                            <h3>Patrocinadores</h3>
                            <div className="textwidget">
                                <p>¿Podemos sonar mejor y quieres que compremos un micro nuevo? ¿Crees que con tu cerveza
							seremos más creativos? ¿Quieres que grabemos en directo en tu restaurante? Escríbenos a <a
                                        href="mailto:hola@tecnologeria.com">hola@tecnologeria.com</a>. Nos dejamos querer :D</p>

                                <p className="text-center"><a href="https://tecnologeria.com/privacidad">Política de privacidad</a>
                                </p>
                            </div>
                        </aside>
                        <aside id="text-2" className="col-md-6 widget widget_text">
                            <div className="textwidget">
                                <div className="text-center footer-follow">
                                    <h3>Síguenos aquí</h3><a href="https://twitter.com/tecnologeria" rel="noopener noreferrer"
                                        target="_blank"><i className="fa fa-3x fa-twitter-square"></i></a>
                                    <a href="https://facebook.com/tecnologeria" rel="noopener noreferrer" target="_blank"><i
                                        className="fa fa-3x fa-facebook-square"></i></a>
                                    <a href="https://www.instagram.com/tecnologeria/" rel="noopener noreferrer"
                                        target="_blank"><i className="fa fa-3x fa-instagram"></i></a>
                                    <a href="https://tecnologeria.com/category/audios/feed/" rel="noopener noreferrer"
                                        target="_blank"><i className="fa fa-3x fa-rss-square"></i></a>
                                    <a href="https://t.me/joinchat/AAAAAEGQNWE_djCJHpenUA" rel="noopener noreferrer"
                                        target="_blank"><i className="fa fa-3x fa-telegram"></i></a>
                                    <a href="https://www.youtube.com/channel/UC_4tdJ6fxE6_WakiZQNOy3g/"
                                        rel="noopener noreferrer" target="_blank"><i className="fa fa-3x fa-youtube-square"></i></a>
                                </div>

                                <div id="mc_embed_signup">
                                    <form
                                        action="https://tecnologeria.us15.list-manage.com/subscribe/post?u=e97790baac27daf8a74c55ddd&amp;id=90926c5874"
                                        method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form"
                                        className="validate" target="_blank">
                                        <div id="mc_embed_signup_scroll">
                                            <h3 htmlFor="mce-EMAIL">¡Suscríbete a La Tecnologería!</h3>
                                            <input type="email" name="EMAIL" className="email" id="mce-EMAIL"
                                                placeholder="tu dirección de e-mail" required="" />
                                            {/* <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups--> */}
                                            <div style={{ position: 'absolute', left: '-5000px' }} aria-hidden="true"><input
                                                type="text" name="b_e97790baac27daf8a74c55ddd_90926c5874" tabIndex="-1"
                                            /></div>
                                            <div className="clear"><input type="submit" value="Suscríbete" name="subscribe"
                                                id="mc-embedded-subscribe" className="btn btn-success button" /></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>

        )
    }

}
export default FooterComp;

