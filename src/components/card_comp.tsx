import * as React from 'react';
import PopupShare from './popUpShare';

interface ICardProps {
    data: any
}

interface ICardState {
    showPopup: boolean
}

class CardComp extends React.Component<ICardProps, ICardState> {

    constructor(props: ICardProps) {
        super(props);
        this.state = { showPopup: false };  
        this.togglePopup = this.togglePopup.bind(this);
        this.likeInc = this.likeInc.bind(this);
    }
     

    togglePopup() {
        this.setState({
          showPopup: !this.state.showPopup,
        });
      }
    
    likeInc() {
        this.props.data.likes=this.props.data.likes+1;
        console.log(this.props.data.likes)
    }

    public render() {    
        const {link, img, episodes, title, description} = this.props.data;        

        return (
            <div className="prog">
                <div className="prog-img">
                    <span className="share-button"><a  onClick={this.togglePopup}><i className="fa fa-share"></i></a></span>
                    <span className="like-button"><a  onClick={this.likeInc}><i className="fa fa-thumbs-up"></i></a></span>
                    <a href={link}><img
                        src={img} alt="" /></a>
                </div>                
                <div className="prog-desc">
                    <div className="prog-header">
                        <h3><a href={link}>{title}</a>
                        </h3>
                        <h4>{episodes}</h4>
                    </div>
                    <p>{description}</p>
                    <a className="episodes-button btn btn-success" href={link}><i
                        className="fa fa-play"></i> Episodios</a>
                    <hr />
                    <div className="prog-listen-buttons">
                        <a className="btn btn-link btn-sm btn-rss"
                            href="https://tecnologeria.com/tecnologeria/feed/"><i className="fa fa-rss"></i></a>
                        <a className="btn btn-link btn-sm btn-ivoox"
                            href="https://www.ivoox.com/podcast-tecnologeria-entrevistas_sq_f1313872_1.html"><i
                                className="fa fa-link"></i></a>
                        <a className="btn btn-link btn-sm btn-itunes"
                            href="https://itunes.apple.com/es/podcast/la-tecnologer%C3%ADa/id1160880131?mt=2"><i
                                className="fa fa-apple"></i></a>
                    </div>
                </div>
            
                {this.state.showPopup ?  
                <PopupShare  
                          link={link}
                          text='Click "Close Button" to hide popup'  
                          closePopup={this.togglePopup.bind(this)}  
                />  
                : null  
                }  
            </div>            
        )
    }

}
export default CardComp;