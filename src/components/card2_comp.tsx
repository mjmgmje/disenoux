import * as React from 'react';
import PopupShare from './popUpShare';

interface ICard2Props {
    data: any
}

interface ICard2State {
    showPopup: boolean
}

class Card2Comp extends React.Component<ICard2Props, ICard2State> {

    constructor(props: ICard2Props) {
        super(props);
        this.state = { showPopup: false };  
        this.togglePopup = this.togglePopup.bind(this);
    }

    togglePopup() {
        this.setState({
          showPopup: !this.state.showPopup,
        });
      }

    public render() {
        const { id, link, img, imgSet, date, title, stitle, episodios } = this.props.data;
        return (
            <article id={id} className="episode">                
                <span className="share-button"><a  onClick={this.togglePopup}><i className="fa fa-share"></i></a></span>
                <figure>                                    
                    <a href={link}><img width="800" height="800"
                    src={img}
                    className="attachment-large size-large wp-post-image" alt=""
                    srcSet={imgSet}
                    sizes="(max-width: 800px) 100vw, 800px" /></a></figure>
                <div className="episode-desc"><span className="podcast-date">{date}</span>
                    <h3 className="podcast-program"><a href="https://tecnologeria.com/101"
                        alt="Ver todas las publicaciones en 101">{title}</a></h3>
                    <h2 className="podcast-title"><a href="https://tecnologeria.com/101/ruby-on-rails"
                        title="Enlace a Ruby on Rails 101" rel="bookmark">{stitle}</a></h2>
                    <h4>{episodios}</h4><a className="play-episode-button btn btn-success"
                        href="https://tecnologeria.com/101/ruby-on-rails"><i className="fa fa-play"></i>
                        Escuchar episodio</a>
                </div>
                {this.state.showPopup ?  
                <PopupShare  
                          link={link}
                          text='Click "Close Button" to hide popup'  
                          closePopup={this.togglePopup.bind(this)}  
                />  
                : null  
                }  
            </article>
        )
    }

}
export default Card2Comp;
