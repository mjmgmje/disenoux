import * as React from 'react';
import TagComp from './tag_comp';

interface ICTagProps {
    data: any;
}

interface ICTagState {
    data: any;

}

class ContTagComp extends React.Component<ICTagProps, ICTagState> {

    constructor(props: ICTagProps) {
        super(props);
    }

    public render() {

        return (
            <div className="row">
                <hr />
                <div className="tecnologeria-playus">

                    {/* TAG */}
                    {
                        this.props.data.map((json: any, index: number) => (
                            <TagComp key={index} data={json}></TagComp>
                        ))
                    }
                </div>
            </div>

        )
    }

}
export default ContTagComp;

