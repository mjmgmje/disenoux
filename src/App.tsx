import React from 'react';
// import logo from './logo.svg';
import './App.css';
import NavComp from './components/nav_comp'
import BannerComp from './components/banner_comp'
import ContCardComp from './components/cont_card_comp';
import BodyCardsMain from './json/BodyCardsMain.json';
import BodyCards2 from './json/BodyCards2.json';
import ContCard2Comp from './components/cont_card2_comp';
import FooterComp from './components/footer_com';
import ContTagComp from './components/cont_tag_comp';
import BodyTag from './json/BodyTag.json';
import TableComp from './components/table_comp';


const App: React.FC = () => {
  return (
    <>
      {/* NAVEGDOR */}
      <NavComp></NavComp>

      {/* CUERPO */}
      {/* BANNER */}
      <BannerComp></BannerComp>

      {/* CONTENEDOR DE TARJETAS */}
      <div className="container dmbs-container">
        {/* <!-- start content container --> */}
        <div className="row dmbs-content">
          <div className="col-md-12 dmbs-main">
            {/* CONTENEDOR CARTAS PRINCIPAL */}
            <ContCardComp data={BodyCardsMain}></ContCardComp>

            <div className="row text-center">
              <h2>Últimos episodios</h2>

              {/* NUEVO CONTENEDOR TARJETAS */}
              <ContCard2Comp data={BodyCards2}></ContCard2Comp>
            </div>

            {/* CONTENEDOR LABELS */}
            <ContTagComp data={BodyTag}></ContTagComp>

            {/* <div className="row text-center">
              <h2>Próximamente</h2> */}

              {/* CONTENEDOR LISTA */}              
              {/* <TableComp data={[["Fecha", "Episodio", "Programa"], ["17-Ene", "1x02", "101", "https://tecnologeria.com/101"], ["24-Ene", "#18", "Vuelo 616: Comic Airlines", "https://tecnologeria.com/vuelo616"], ["31-Ene", "3x03", "3 al Cubo", "https://tecnologeria.com/3alcubo"]
, ["7-Feb", "8x01", "RUN: los videojuegos desde dentro", "https://tecnologeria.com/run"], ["14-Feb", "8x01", "La Tecnologería Entrevistas", "https://tecnologeria.com/tecnologeria"]]}></TableComp>
            </div> */}
            <div className="clear"></div>
            <div className="dmbs-comments">
              <a name="comments"></a>
            </div>


          </div>
        </div>
        <div className="dmbs-footer">
        </div>
      </div>

      {/* FOOTER */}

      <FooterComp></FooterComp>

      {/* CARGA SCRIPTS */}


      <script type="text/javascript" src="./assets/js/scripts.js"></script>
      <script type="text/javascript" src="./assets/js/api.js"></script>
      <script type="text/javascript" src="./assets/js/custom.min.js"></script>
      <script type="text/javascript" src="./assets/js/bootstrap.js"></script>
      <script type="text/javascript" src="./assets/js/wp-embed.min.js"></script>

      <link rel="stylesheet" type="text/css" href="./assets/css/cookieconsent.min.css" />
      <script src="./assets/js/cookieconsent.min.js"></script>
      <div>
        <div className="grecaptcha-badge" data-style="bottomright"
          style={{ width: '256px', height: '60px', display: 'block', transition: 'right 0.3s ease 0s', position: 'fixed', bottom: '14px', right: '-186px', boxShadow: 'gray 0px 0px 5px', borderRadius: '2px', overflow: 'hidden' }}>
          <div className="grecaptcha-logo"><iframe src="./assets/html/anchor.html" width="256"
            height="60" role="presentation" name="a-qtc9h9m2roqe" frameBorder="0" scrolling="no"
            sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe>
          </div>
          <div className="grecaptcha-error"></div><textarea id="g-recaptcha-response-100000" name="g-recaptcha-response"
            className="g-recaptcha-response"
            style={{ width: '250px', height: '40px', border: '1px solid rgb(193, 193, 193)', margin: '10px 25px', padding: '0px', resize: 'none', display: 'none' }}></textarea>
        </div>
      </div><iframe scrolling="no" frameBorder="0" allowtransparency="true"
        src="./assets/html/widget_iframe.4f8aea4342a4ada72cba2bdffcff6b4d.html"
        title="Twitter settings iframe" style={{ display: 'none' }}></iframe>
      <script type="text/javascript" async=""
        src="./assets/js/212b3d4039ab5319ec.js"></script><iframe
          name="_hjRemoteVarsFrame" title="_hjRemoteVarsFrame" id="_hjRemoteVarsFrame"
          src="./assets/html/box-b736908ce6b0e933fad3a2e45df61b38.html"
          style={{ display: 'none !important', width: '1px !important', height: '1px !important', opacity: '0 !important' }}></iframe>
    </>
  );
}
export default App;
